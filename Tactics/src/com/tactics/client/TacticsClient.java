package com.tactics.client;

import java.io.IOException;
import java.util.ArrayList;

import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Listener;
import com.tactics.Tactics;
import com.tactics.Screens.Lobby;
import com.tactics.game.logic.GameLogic;
import com.tactics.game.logic.Team;
import com.tactics.game.networking.Network;
import com.tactics.game.networking.packets.AttackRequestPacket;
import com.tactics.game.networking.packets.MovementPacket;
import com.tactics.game.networking.packets.Packet;
import com.tactics.game.networking.packets.StandbyPacket;
import com.tactics.game.unit.Unit;
import com.tactics.lobby.LobbyState;
import com.tactics.lobby.networking.AddUnitPacket;
import com.tactics.lobby.networking.ReadyPacket;
import com.tactics.lobby.networking.RemoveUnitPacket;
import com.tactics.lobby.networking.StartPacket;

public class TacticsClient {

	GameLogic gameLogic;
	LobbyState lobbyState;
	Client client;
	Listener listener;
	int id;
	Tactics t;
	
	public TacticsClient (Tactics t) {
		this.t = t;
	}
	
	public void initGame(ArrayList<Team> teams) {
		t.swapLobbyAndGame(teams);
	}
	
	public void connect(final String host, final int port) {
		client = new Client();
		client.start();
		Network.register(client);
		
		try {
			client.connect(5000, host, port);
			// Server communication after connection can go here, or in Listener#connected().
		} catch (IOException ex) {
			ex.printStackTrace();
			System.exit(1);
		}
		System.out.println("client id " + client.getID());
		id = client.getID();
	}

	public void switchToLobby() {
		if(listener != null)
			client.removeListener(listener);
		listener = new TacticsClientLobbyListener(this, (Lobby) t.getScreen());
		client.addListener(listener);
		
		lobbyState = new LobbyState();
	}
	
	public void switchToGame(GameLogic logic) {
		gameLogic = logic;
		
		if(listener != null)
			client.removeListener(listener);
		listener = new TacticsClientGameListener(this, gameLogic);
		client.addListener(listener);
	}
	
	public void addLobbyUnit(String unitType) {
		lobbyState.addUnit(unitType, 1);
		AddUnitPacket packet = new AddUnitPacket(unitType, id);
		sendPacket(packet);
	}
	
	public void removeLobbyUnit(int unitId) {
		lobbyState.removeUnit(unitId, 1);
		RemoveUnitPacket packet = new RemoveUnitPacket(unitId, id);
		sendPacket(packet);
	}
	
	public void sendMovePacket(Unit unit, int newX, int newY) {
		MovementPacket packet = new MovementPacket(unit, newX, newY);
		sendPacket(packet);
	}
	
	public void sendStanbyPacket(Unit unit) {
		StandbyPacket packet = new StandbyPacket(unit);
		sendPacket(packet);
	}
	
	public void sendAttackPacket(Unit attacker, Unit attacked) {
		if(attacker == null || attacked == null)
			return;
		AttackRequestPacket packet = new AttackRequestPacket(attacker, attacked);
		sendPacket(packet);
	}
	
	public void sendStartPacket() {
		StartPacket packet = new StartPacket();
		sendPacket(packet);
	}
	
	public void sendReadyPacket(boolean ready) {
		System.out.println("Sending Ready Packet");
		ReadyPacket packet = new ReadyPacket(ready);
		sendPacket(packet);
	}
	
	public void sendPacket(Packet packet) {
		client.sendTCP(packet);
	}
	
	public void setId(int newId) {
		this.id = newId;
	}
	
	public int getId() {
		return id;
	}
}
