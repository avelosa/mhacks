package com.tactics.client;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.tactics.game.logic.GameLogic;
import com.tactics.game.networking.packets.AttackResponsePacket;
import com.tactics.game.networking.packets.CooldownPacket;
import com.tactics.game.networking.packets.DeathPacket;
import com.tactics.game.networking.packets.GameLostPacket;
import com.tactics.game.networking.packets.MovementPacket;
import com.tactics.game.networking.packets.OffCooldownPacket;
import com.tactics.game.unit.Unit;



public class TacticsClientGameListener extends Listener {
	TacticsClient client;
	GameLogic gameLogic;
	
	public void connected (Connection connection) {

	}
	
	public TacticsClientGameListener(TacticsClient client, GameLogic logic) {
		this.client = client;
		gameLogic = logic;
	}
	
	public void received (Connection c, Object object) { 
		if(object instanceof MovementPacket) {
			MovementPacket packet = (MovementPacket) object;
			
			if(packet.isSuccessful()) {
				Unit unit = gameLogic.getUnitFromTeam(packet.getUnitTeam(), packet.getUnitId());
				
				int newX = packet.getNewX();
				int newY = packet.getNewY();
				
				unit.teleport(gameLogic, newX, newY);
			} else {
				//ERROR
			}
		} else if(object instanceof DeathPacket) {
			DeathPacket packet = (DeathPacket) object;
			Unit unit = gameLogic.getUnitFromTeam(packet.getUnitTeam(), packet.getUnitId());
			unit.setDead(true);
			unit.getTile().setUnit(null);
		} else if(object instanceof CooldownPacket) {
			CooldownPacket packet = (CooldownPacket) object;
			Unit unit = gameLogic.getUnitFromTeam(packet.getUnitTeam(), packet.getUnitId());
			unit.setCanAct(packet.canAct());
			unit.setCanMove(packet.canMove());
			unit.setCanAttack(packet.canAttack());
			unit.setCurCooldown(packet.getCurCooldown());
		} else if(object instanceof OffCooldownPacket) {
			OffCooldownPacket packet = (OffCooldownPacket) object;
			Unit unit = gameLogic.getUnitFromTeam(packet.getUnitTeam(), packet.getUnitId());
			unit.setCanAct(packet.isOffCooldown());
			unit.setCurCooldown(0);
		} else if(object instanceof AttackResponsePacket) {
			System.out.println("someone got attaced");
			AttackResponsePacket packet = (AttackResponsePacket) object;
			
			Unit attacker = gameLogic.getUnitFromTeam(packet.getAttackerTeam(), packet.getAttackerId());
			Unit attacked = gameLogic.getUnitFromTeam(packet.getAttackedTeam(), packet.getAttackedId());
			
			if(packet.isSuccessful()){
				System.out.println("Was successful + " + packet.getAttackedHealth());
				attacked.setCurHealth(packet.getAttackedHealth());
			}
		} else if(object instanceof GameLostPacket) {
			System.out.println("GAME LOST");
			GameLostPacket packet = (GameLostPacket) object;
			if(client.getId() == packet.getLooserTeamId()) {
				//this person lost
			} else {
				//this person won
			}
		}
	}
	
	public void disconnected (Connection c) {
	}
}
