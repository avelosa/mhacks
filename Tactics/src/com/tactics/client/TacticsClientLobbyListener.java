package com.tactics.client;

import java.util.ArrayList;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.tactics.Screens.Lobby;
import com.tactics.game.logic.Team;
import com.tactics.game.networking.packets.GameInitializationPacket;
import com.tactics.lobby.networking.PlayerJoinedPacket;
import com.tactics.lobby.networking.ReadyPacket;
import com.tactics.server.PlayerConnection;

public class TacticsClientLobbyListener extends Listener {
	TacticsClient client;
	//used for building game logic
	ArrayList<Team> teams;
	Lobby lobby;
	
	public void connected (Connection connection) {

	}
	
	public TacticsClientLobbyListener(TacticsClient client, Lobby lobby) {
		this.client = client;
		teams = new ArrayList<Team>();
		this.lobby = lobby;
	}
	
	public void received (Connection c, Object object) {
		if(object instanceof PlayerJoinedPacket) {
			//Notify the GUI
		} else if (object instanceof ReadyPacket) {
			System.out.println("IT GOT TO ITS FINAL DESTINATION " + client.getId() );
			ReadyPacket packet = (ReadyPacket) object;
			if(packet.isReady()) {
				lobby.otherPlayerIsReady(true);
			} else {
				lobby.otherPlayerIsReady(false);
			} 
		}  else if(object instanceof GameInitializationPacket) {
			System.out.println("Got a game init packet");
			GameInitializationPacket packet = (GameInitializationPacket) object;
			teams.add(packet.getTeam());
			
			if(packet.getTeamId() == 2 ) {	
				client.initGame(teams);
			}
		}
	}
	
	public void disconnected (Connection c) {
		//PlayerConnection connection = (PlayerConnection) c;
	}
}
