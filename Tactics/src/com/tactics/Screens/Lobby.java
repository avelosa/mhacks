package com.tactics.Screens;

import java.awt.Font;
import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.tactics.Tactics;
import com.tactics.Sprites.TacticsSprite;
import com.tactics.Sprites.Icons.IconPurpleArcher;
import com.tactics.Sprites.Icons.IconPurpleMilitia;
import com.tactics.Sprites.Icons.IconPurplePaladin;
import com.tactics.Sprites.Icons.IconRedMilitia;
import com.tactics.client.TacticsClient;
import com.tactics.game.gui.GenericGUI;
import com.tactics.game.logic.Team;
import com.tactics.game.unit.UnitTypes;

public class Lobby implements Screen, InputProcessor {

	private SpriteBatch batch;
	private OrthographicCamera camera;
	private GenericGUI gui;
	private Team team;
	private int teamNumber;
	private TacticsClient client;
	private Tactics tactics;
	private TacticsSprite pala;
	private TacticsSprite mili;
	private TacticsSprite arch;
	private ArrayList<TacticsSprite> spriteDraw;
	
	private int armyPoints;
	
	private BitmapFont font;
	
	private final int paladinPoints = 2;
	private final int militiaPoints = 1;
	private final int archerPoints = 2;
	
	private boolean goToGame;
	
	private boolean runningServer;
	private boolean otherPlayerReady;
	private boolean launchGame = false;
	
	public void goToGame() {
		goToGame = true;
	}
	
	public Lobby(TacticsClient client, Tactics tactics, boolean runningServer) {
		this.client = client;
		goToGame = false;
		this.tactics = tactics;
		this.runningServer = runningServer;
		otherPlayerReady = false;
	}
	
	public void setClient(TacticsClient client) {
		this.client = client;
		
		System.out.println("client connected " + client.getId() + " " + this.runningServer);
	}
	
	public void otherPlayerIsReady(boolean ready) {
		otherPlayerReady = ready;
	}
		
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0 , 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		batch.begin();
		gui.draw(batch);
		pala.draw(batch, 117, 345);
		mili.draw(batch, 117, 265);
		arch.draw(batch, 117, 185);
		font.draw(batch, Integer.toString(armyPoints) + " Points Remaining.", 500, 450);
		font.draw(batch, "Knight, 2 Points", 200, 375);
		font.draw(batch, "Militia, 1 Point", 200, 300);
		font.draw(batch, "Archer, 2 Points", 200, 225);
		drawArmy();
		batch.end();
		
		if(launchGame && runningServer && otherPlayerReady) {
			//this.dispose();
			System.out.println("SENDING START");
			client.sendStartPacket();
			launchGame = false;
		} else if (launchGame && !runningServer) {
			client.sendReadyPacket(true);
			launchGame = false;
		}
		
		if(goToGame)
			tactics.startGame();
		
	}
	
	public void drawArmy() {
		for(int i = 0; i < spriteDraw.size(); i++) {
			spriteDraw.get(i).draw(batch, 425 + (i%3)*86, 350 - (i/3)*86);
		}
	}

	@Override
	public void resize(int width, int height) {
		//camera.position.x -= 62;
		camera.viewportWidth = width;
		camera.viewportHeight = height;
	}

	@Override
	public void show() {
		camera = new OrthographicCamera();
		batch = new SpriteBatch();
		gui = new GenericGUI(new Sprite(new Texture("data/gui.png")));
		pala = new IconPurplePaladin();
		mili = new IconPurpleMilitia();
		arch = new IconPurpleArcher();
		
		font = new BitmapFont();
		
		armyPoints = 9;
		
		spriteDraw = new ArrayList<TacticsSprite>();
		
		Gdx.input.setInputProcessor(this);
		
	}

	@Override
	public void hide() {
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		batch.dispose();
		gui.getTexture().dispose();
		mili.getTexture().dispose();
		pala.getTexture().dispose();
		arch.getTexture().dispose();
		
	}

	@Override
	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		//launchGame = true;
		//add buttons
		int size = spriteDraw.size();
		
		if(armyPoints > 0) {
			if((screenX >= 127 && screenX <= 169) && (screenY >= 238 && screenY <= 280)) {
				if(armyPoints - paladinPoints >= 0) {
					client.addLobbyUnit(UnitTypes.Paladin.toString());
					spriteDraw.add(pala);
					armyPoints -= paladinPoints;
				}
			}
			if((screenX >= 127 && screenX <= 169) && (screenY >= 324 && screenY <= 366)) {
				if(armyPoints - militiaPoints >= 0) {
					client.addLobbyUnit(UnitTypes.Militia.toString());
					spriteDraw.add(mili);
					armyPoints -= militiaPoints;
				}
			}
			if((screenX >= 127 && screenX <= 169) && (screenY >= 410 && screenY <= 452)) {
				if(armyPoints - archerPoints >= 0) {
					client.addLobbyUnit(UnitTypes.Archer.toString());
					spriteDraw.add(arch);
					armyPoints -= archerPoints;
				}
			}
		}
		
		//ready button
		if((screenX >= 765 && screenX <= 955) && (screenY >= 511 && screenY <= 596))
			launchGame = true;
		
		//remove buttons
		if((screenX >= 435 && screenX <= 477) && (screenY >= 238 && screenY <= 280)) {
			client.removeLobbyUnit(0);
			if(size >= 1) {
				if(spriteDraw.remove(0).equals(mili))
					armyPoints += 1;
				else
					armyPoints += 2;	
			}
		}
		if((screenX >= 521 && screenX <= 563) && (screenY >= 238 && screenY <= 280)) {
			client.removeLobbyUnit(1);
			if(size >= 2) {
				if(spriteDraw.remove(1).equals(mili))
					armyPoints += 1;
				else
					armyPoints += 2;	
			}
		}
		if((screenX >= 607 && screenX <= 649) && (screenY >= 238 && screenY <= 280)) {
			client.removeLobbyUnit(2);
			if(size >= 3) {
				if(spriteDraw.remove(2).equals(mili))
					armyPoints += 1;
				else
					armyPoints += 2;	
			}
		}
		if((screenX >= 435 && screenX <= 477) && (screenY >= 324 && screenY <= 366)) {
			client.removeLobbyUnit(3);
			if(size >= 4) {
				if(spriteDraw.remove(3).equals(mili))
					armyPoints += 1;
				else
					armyPoints += 2;	
			}
		}
		if((screenX >= 521 && screenX <= 563) && (screenY >= 324 && screenY <= 366)) {
			client.removeLobbyUnit(4);
			if(size >= 5) {
				if(spriteDraw.remove(4).equals(mili))
					armyPoints += 1;
				else
					armyPoints += 2;	
			}
		}
		if((screenX >= 607 && screenX <= 649) && (screenY >= 324 && screenY <= 366)) {
			client.removeLobbyUnit(5);
			if(size >= 6) {
				if(spriteDraw.remove(5).equals(mili))
					armyPoints += 1;
				else
					armyPoints += 2;	
			}
		}
		if((screenX >= 435 && screenX <= 477) && (screenY >= 410 && screenY <= 452)) {
			client.removeLobbyUnit(6);
			if(size >= 7) {
				if(spriteDraw.remove(6).equals(mili))
					armyPoints += 1;
				else
					armyPoints += 2;	
			}
		}
		if((screenX >= 521 && screenX <= 563) && (screenY >= 410 && screenY <= 452)) {
			client.removeLobbyUnit(7);
			if(size >= 8) {
				if(spriteDraw.remove(7).equals(mili))
					armyPoints += 1;
				else
					armyPoints += 2;	
			}		}
		if((screenX >= 607 && screenX <= 649) && (screenY >= 410 && screenY <= 452)) {
			client.removeLobbyUnit(8);
			if(size >= 9) {
				if(spriteDraw.remove(8).equals(mili))
					armyPoints += 1;
				else
					armyPoints += 2;	
			}
		}

		return true;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}
	
	public boolean launchGame() {
		return this.launchGame;
	}

}
