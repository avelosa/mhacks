package com.tactics.Screens;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.tactics.Sprites.SpriteManager;
import com.tactics.Tiles.Tile;
import com.tactics.Tiles.TileManager;
import com.tactics.client.TacticsClient;
import com.tactics.game.logic.GameLogic;
import com.tactics.game.logic.Team;
import com.tactics.game.unit.Unit;
import com.tactics.server.TacticsServer;

public class Play implements Screen, InputProcessor{

	private TiledMap map;
	private OrthogonalTiledMapRenderer renderer;
	private	OrthographicCamera camera;
	private int team;

	private GameLogic gameLogic;
	private GameLogic serverGameLogic;

	private TacticsClient client;
	private TacticsServer server;

	private long lastTick;

	private static final int TILE_SIZE = 64;
	private static final int UI_WIDTH = 200;
	private static final float SCROLL_INTERVAL = TILE_SIZE * 5;
	private static final int SCROLL_BUFFER = TILE_SIZE;

	private float totalWidth;
	private float totalHeight;

	private float shownWidth;
	private float shownHeight;

	private boolean hasSelected;
	private Unit selectedUnit;
	private ArrayList<Tile> highlightedTiles;

	private boolean forceMove;
	private boolean forceAttack;

	private boolean scrollLeft;
	private boolean scrollRight;
	private boolean scrollUp;
	private boolean scrollDown;

	private int currentMouseX;
	private int currentMouseY;

	private BitmapFont font;
	
	public Play(GameLogic logic, TacticsClient client, TacticsServer server) {
		this.gameLogic = logic;
		lastTick = System.currentTimeMillis();
		scrollLeft = false;
		scrollRight = false;
		scrollUp = false;
		scrollDown = false;
		hasSelected = false;
		forceMove = false;
		forceAttack = false;
		this.client = client;
		this.team = client.getId();
		
		this.server = server;
	}

	public Play(GameLogic logic, TacticsClient client, TacticsServer server, GameLogic serverLogic) {
		this(logic, client, server);
		this.serverGameLogic = serverLogic;
		this.team = client.getId();
		//render(1F);
	}

	public void update() {
		long currentTick = System.currentTimeMillis();
		long interval = currentTick - lastTick;
		lastTick = currentTick;


		gameLogic.update(interval);	
		if(serverGameLogic != null){
			serverGameLogic.update(interval);
			//Team team = serverGameLogic.checkForLooser();
			//if(team != null) {
			//	server.sendGameLostPacket(team);
			//}
		}

		scroll(interval);
	}

	private void scroll(long interval) {
		float tempX = camera.position.x;
		float tempY = camera.position.y;

		if (scrollLeft) {
			tempX -= (interval / 1000F) * SCROLL_INTERVAL;
		}
		if (scrollRight) {
			tempX += (interval / 1000F) * SCROLL_INTERVAL;
		}
		if (scrollUp) {
			tempY += (interval / 1000F) * SCROLL_INTERVAL;
		}
		if (scrollDown) {
			tempY -= (interval / 1000F) * SCROLL_INTERVAL;
		}
		if (tempX <= (shownWidth / 2))
			tempX = (shownWidth / 2);
		if (tempX >= totalWidth - (shownWidth / 2) + UI_WIDTH) 
			tempX = totalWidth - (shownWidth / 2) + UI_WIDTH;
		if (tempY <= (shownHeight / 2)) 
			tempY = (shownHeight / 2);
		if (tempY >= totalHeight - (shownHeight / 2))
			tempY = totalHeight - (shownHeight / 2);

		tempX = (float) Math.floor(tempX);
		tempY = (float) Math.floor(tempY);

		camera.position.x = tempX;
		camera.position.y = tempY;

		camera.update();

	}

	public void render(float delta) {


		Gdx.gl.glClearColor(0, 0, 0 , 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		renderer.setView(camera);
		renderer.render();
		
		update();

		update();
		
		SpriteBatch batch = renderer.getSpriteBatch();

		batch.begin();

		if (highlightedTiles != null) {
			for (int i = 0; i < highlightedTiles.size(); i++) {
				Tile tempTile = highlightedTiles.get(i);
				if (forceMove) {
					SpriteManager.BLUEMASK.draw(batch, tempTile.getX() * TILE_SIZE, tempTile.getY() * TILE_SIZE);
				} else if (forceAttack) {
					SpriteManager.REDMASK.draw(batch, tempTile.getX() * TILE_SIZE, tempTile.getY() * TILE_SIZE);
				}
			}
		}

		ArrayList<Team> teams = gameLogic.getTeams();
		for (int i = 0; i < teams.size(); i++) {
			ArrayList<Unit> temp = teams.get(i).getUnits();
			for (int j = 0; j < temp.size(); j++) {

				Unit unit = temp.get(j);
				if (unit.canAct()) {
					unit.getSprite().setColor(Color.WHITE); 
				} else {
					unit.getSprite().setColor(Color.GRAY);
				}
				if(!unit.isDead()) {
					unit.getSprite().draw(batch, unit.getX() * TILE_SIZE, unit.getY() * TILE_SIZE);
					font.draw(batch, "HP: " + unit.getCurHealth() + "/" + unit.getHp(), unit.getX() * TILE_SIZE, unit.getY() * TILE_SIZE);
				}

			}
		}

		Tile temp = getCurrentHighlightedTile();

		if (temp != null) {
			SpriteManager.OUTLINE.draw(batch, temp.getX() * TILE_SIZE, temp.getY() * TILE_SIZE);
		}

		renderer.getSpriteBatch().end();

		//System.out.printf("%f %f\n", camera.position.x, camera.position.y);
	}

	@Override
	public void resize(int width, int height) {		
		camera.viewportWidth = width;
		camera.viewportHeight = height;
		camera.position.x = 210;
		camera.position.y = 160;

		shownWidth = camera.viewportWidth;
		shownHeight = camera.viewportHeight;

		camera.update();
	}

	@Override
	public void show() {

		map = new TmxMapLoader().load("maps/map.tmx");
		font = new BitmapFont();
		renderer = new OrthogonalTiledMapRenderer(map);

		camera = new OrthographicCamera();

        camera.update();

		totalWidth = ((Integer) map.getProperties().get("width")) * TILE_SIZE;
		totalHeight = ((Integer) map.getProperties().get("height")) * TILE_SIZE;

		currentMouseX = (int) camera.position.x;
		currentMouseY = (int) camera.position.y;

		createTileArray();

		Gdx.input.setInputProcessor(this);
	}

	public Tile getCurrentHighlightedTile() {

		int gridX = (int) (camera.position.x - shownWidth / 2);
		gridX += currentMouseX;
		gridX /= TILE_SIZE;
		int gridY = (int) (camera.position.y - shownHeight / 2);
		gridY += (shownHeight - currentMouseY);
		gridY /= TILE_SIZE;

		if (gridX >= ((totalWidth) / TILE_SIZE))
			gridX = (int) ((totalWidth) / TILE_SIZE) - 1;
		if (gridX < 0)
			gridX = 0;
		if (gridY >= (totalHeight / TILE_SIZE)) 
			gridY = (int) ((totalHeight / TILE_SIZE) - 1);
		if (gridY < 0)
			gridY = 0;

		return gameLogic.getTiles()[gridX][gridY];
	}

	private void createTileArray() {
		int width = (int) (totalWidth / TILE_SIZE);
		int height = (int) (totalHeight / TILE_SIZE);

		Tile[][] tiles = new Tile[width][height];

		TiledMapTileLayer layer = (TiledMapTileLayer) map.getLayers().get(0);

		for (int i = 0; i  < height; i++) {
			for (int j = 0; j < width; j++) {
				int id = layer.getCell(j, i).getTile().getId();
				boolean pathable = false;
				if (TileManager.pathing.get(id) == null) {
					pathable = true;
				}
				tiles[j][i] = new Tile(j, i, pathable);
			}
		}

		gameLogic.setTiles(tiles);
		if(serverGameLogic != null)
			serverGameLogic.setTiles(copyTileArray(tiles));
	}

	private Tile[][] copyTileArray(Tile[][] orig) {
		int width = (int) (totalWidth / TILE_SIZE);
		int height = (int) (totalHeight / TILE_SIZE);

		Tile[][] tiles = new Tile[width][height];
		for (int i = 0; i  < height; i++) {
			for (int j = 0; j < width; j++) {
				tiles[j][i] = new Tile(j, i, orig[j][i].isWalkable());
			}
		}
		return tiles;		
	}

	@Override
	public void hide() {

	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void dispose() {
		map.dispose();
		renderer.dispose();
	}

	@Override
	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		if (character == 's' && this.selectedUnit != null) {
			this.client.sendStanbyPacket(this.selectedUnit);
			this.highlightedTiles = null;
			this.hasSelected = false;
			this.selectedUnit = null;
			this.forceMove = false;
		} else if (character == 'a' && this.selectedUnit != null) {
			handleAttack(selectedUnit.getX(), selectedUnit.getY());
		}
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		if (screenX - (shownWidth - UI_WIDTH) >= 0) {
			handleUIClick(screenX, screenY);
		} else {
			handleGridClick(screenX, screenY);
		}
		return false;
	}

	private void handleUIClick(int screenX, int screenY) {

	}

	private void handleGridClick(int screenX, int screenY) {
		int gridX = (int) (camera.position.x - shownWidth / 2);
		gridX += screenX;
		gridX /= TILE_SIZE;
		int gridY = (int) (camera.position.y - shownHeight / 2);
		gridY += (shownHeight - screenY);
		gridY /= TILE_SIZE;

		if (forceMove && selectedUnit.canMove()) {
			System.out.println("moving");
			handleMovement(gridX, gridY);
			return;
		} else if (forceAttack) {
			System.out.println("attacking");
			client.sendAttackPacket(selectedUnit, gameLogic.getUnit(gridX, gridY));
			client.sendStanbyPacket(selectedUnit);
			highlightedTiles = null;
			hasSelected = false;
			forceAttack  = false;
			selectedUnit = null;
			return;
		} else if (hasSelected) {
			System.out.println("deselecting");
			selectedUnit.deselect();
			selectedUnit = null;
			highlightedTiles = null;
			hasSelected = false;
			forceMove = false;
		}

		Tile tempTile = gameLogic.getTiles()[gridX][gridY];

		if (tempTile.getUnit() != null && tempTile.getUnit().getTeam().getTeam() == this.team) {
			selectedUnit = tempTile.getUnit();

			selectedUnit.select();
			hasSelected = true;
			forceMove = true;
			highlightedTiles = gameLogic.getTilesWithinRange(2, tempTile);
		}

	}

	public boolean isPathable(int x, int y) {
		Tile tempTile = gameLogic.getTiles()[x][y];
		return tempTile.isWalkable();
	}

	public void handleAttack(int x, int y) {
		ArrayList<Tile> tiles = gameLogic.getTilesWithinRangeIgnoreBlocks(selectedUnit.getRange(), selectedUnit.getTile());
		int tempTeam = selectedUnit.getTeam().getTeamNumber();
		for (int i = 0; i < tiles.size(); i++) {
			Tile tile = tiles.get(i);
			if (tile.getUnit() == null || tile.getUnit().getTeam().getTeamNumber() == tempTeam) {
				tiles.remove(i);
				i--;
			}
		}

		highlightedTiles = tiles;
		forceAttack = true;
		forceMove = false;
	}

	public void handleMovement(int x, int y) {
		if (!isPathable(x, y) || !gameLogic.getTiles()[x][y].isEmpty()) {
			System.out.println("Can't move there");
			return;
		}
		//selectedUnit.setTile(gameLogic.getTiles()[x][y]);
		//gameLogic.getTiles()[x][y].setUnit(selectedUnit);

		if (!highlightedTiles.contains(gameLogic.getTiles()[x][y])) {
			return;
		}
		System.out.println("Somehow we got in here.");
		client.sendMovePacket(selectedUnit, x, y);

		selectedUnit.setTile(gameLogic.getTiles()[x][y]);
		gameLogic.getTiles()[x][y].setUnit(selectedUnit);
		forceMove = false;
		selectedUnit = null;
		hasSelected = false;
		highlightedTiles = null;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		if (screenX <= SCROLL_BUFFER) {
			this.scrollLeft = true;
		} else {
			this.scrollLeft = false;
		}
		if (screenX >= camera.viewportWidth - (SCROLL_BUFFER)) {
			this.scrollRight = true;
		} else {
			this.scrollRight = false;
		}
		if (screenY <= SCROLL_BUFFER && screenX <= camera.viewportWidth - (UI_WIDTH)) {
			this.scrollUp = true;
		} else {
			this.scrollUp = false;
		}
		if (screenY >= (camera.viewportHeight - SCROLL_BUFFER) && screenX <= camera.viewportWidth - (UI_WIDTH)) {
			this.scrollDown = true;
		} else {
			this.scrollDown = false;
		}

		currentMouseX = screenX;
		currentMouseY = screenY;

		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

}
