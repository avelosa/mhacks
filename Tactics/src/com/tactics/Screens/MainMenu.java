package com.tactics.Screens;

import java.awt.Point;
import java.awt.Rectangle;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.TextInputListener;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.tactics.Tactics;
import com.tactics.Sprites.SpriteMainMenu;
import com.tactics.game.networking.Network;

public class MainMenu implements Screen, InputProcessor {

	private OrthogonalTiledMapRenderer renderer;
	private	OrthographicCamera camera;
	private SpriteMainMenu sprite;
	private Rectangle host;
	private Rectangle join;
	private Tactics tactics;
	
	public MainMenu(Tactics tactics){
		this.tactics = tactics;
	}
	
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0 , 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		SpriteBatch batch = new SpriteBatch();

		batch.begin();
		sprite.draw(batch, 0, 0);
		batch.end();
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		//renderer = new OrthogonalTiledMapRenderer(null);

		camera = new OrthographicCamera();

		camera.update();
		sprite = new SpriteMainMenu();

		host = new Rectangle(387, 322, 288, 70);
		join = new Rectangle(387, 453, 288, 70);

		Gdx.input.setInputProcessor(this);
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		sprite.getTexture().dispose();
	}

	@Override
	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		Point point = new Point(screenX, screenY);
		if (host.contains(point)) {
			host();
		} else if (join.contains(point)) {
			join();
		}
		return false;
	}

	public void host() {
		tactics.startLobby(true, "localhost", Network.DEFAULT_PORT);
	}

	public void join() {
		System.out.println("Joining :(");
		
		class InputFrame implements TextInputListener {
			public String ip;
			
			public void input (String text) {
				this.ip = text;
//				String ip = listener.ip;
				tactics.startLobby(false, ip, Network.DEFAULT_PORT);
			}

			public void canceled () {
			}
			
			public String getIp() {
				return this.ip;
			}
		}

		InputFrame listener = new InputFrame();
		Gdx.input.getTextInput(listener, "IP Address", "Please enter an IP address");
		
		
		
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

}
