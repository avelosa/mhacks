package com.tactics.networking;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.tactics.game.unit.Archer;

public class ArcherSerializer extends Serializer<Archer>{

	@Override
	public Archer read(Kryo kryo, Input input, Class<Archer> type) {
		Archer archer = new Archer(0);
		archer.setX(input.readInt());
		archer.setY(input.readInt());
		
		return archer;
	}

	@Override
	public void write(Kryo kryo, Output output, Archer archer) {
		output.write(archer.getX());
		output.write(archer.getY());
		
	}

}
