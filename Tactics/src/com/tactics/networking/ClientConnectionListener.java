package com.tactics.networking;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.tactics.client.TacticsClient;

public class ClientConnectionListener extends Listener{
	private TacticsClient client;

	public ClientConnectionListener(TacticsClient client) {
		this.client = client;
	}

	public void connected (Connection connection) {
		client.setId(connection.getID());
	}
	
	
}
