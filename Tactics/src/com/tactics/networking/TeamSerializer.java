package com.tactics.networking;

import java.util.ArrayList;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.tactics.game.logic.Team;
import com.tactics.game.unit.Unit;

public class TeamSerializer extends Serializer<Team> {

	@Override
	public Team read(Kryo kryo, Input input, Class<Team> type) {
		String name = input.readString();
		int number = input.readInt();
		Team team = new Team(name, number);

		ArrayList<Unit> units = (ArrayList<Unit>) kryo.readClassAndObject(input);
		for(Unit unit:units){
			team.addUnit(unit);
		}
		return team;
	}

	@Override
	public void write(Kryo kryo, Output output, Team team) {
		output.writeString(team.getTeamName());
		output.writeInt(team.getTeamNumber());
		
		kryo.writeClassAndObject(output, team.getUnits());
	}

}
