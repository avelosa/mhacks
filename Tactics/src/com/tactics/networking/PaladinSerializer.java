package com.tactics.networking;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.tactics.game.unit.Paladin;

public class PaladinSerializer extends Serializer<Paladin> {

	@Override
	public Paladin read(Kryo kryo, Input input, Class<Paladin> type) {
		Paladin paladin = new Paladin();
		paladin.setX(input.readInt());
		paladin.setY(input.readInt());
		
		return paladin;
	}

	@Override
	public void write(Kryo kryo, Output output, Paladin paladin) {
		output.writeInt(paladin.getX());
		output.writeInt(paladin.getY());
	}

}
