package com.tactics.networking;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.tactics.game.unit.Militia;

public class MilitiaSerializer extends Serializer<Militia> {

	@Override
	public Militia read(Kryo kryo, Input input, Class<Militia> type) {
		Militia militia = new Militia();
		militia.setX(input.readInt());
		militia.setY(input.readInt());
		
		return militia;
	}

	@Override
	public void write(Kryo kryo, Output output, Militia militia) {
		output.writeInt(militia.getX());
		output.writeInt(militia.getY());
	}

}
