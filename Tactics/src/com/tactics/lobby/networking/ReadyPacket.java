package com.tactics.lobby.networking;

import com.tactics.game.networking.packets.Packet;

public class ReadyPacket implements Packet{
	public boolean ready;
	
	public ReadyPacket() {
	}
	
	public ReadyPacket(boolean ready){
		this.ready = ready;
	}
	
	public boolean isReady() {
		return ready;
	}
}
