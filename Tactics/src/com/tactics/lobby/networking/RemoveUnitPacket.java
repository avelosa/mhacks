package com.tactics.lobby.networking;

import com.tactics.game.networking.packets.Packet;

public class RemoveUnitPacket implements Packet {
	public int unitId;
	public int unitTeam;
	
	public RemoveUnitPacket(){
	}
	
	public RemoveUnitPacket(int unitId, int unitTeam) {
		this.unitId = unitId;
		this.unitTeam = unitTeam;
	}
	
	public int getUnitId() {
		return unitId;
	}
	
	public int getUnitTeam() {
		return unitTeam;
	}
	

}
