package com.tactics.lobby.networking;

import com.tactics.game.networking.packets.Packet;

public class AddUnitPacket implements Packet {
	public String unitType;
	public int unitTeam;
	
	public AddUnitPacket(){
	}
	
	public AddUnitPacket(String unitType, int unitTeam) {
		this.unitType = unitType;
		this.unitTeam = unitTeam;
	}
	
	public String getUnitType() {
		return unitType;
	}
	
	public int getUnitTeam() {
		return unitTeam;
	}
}
