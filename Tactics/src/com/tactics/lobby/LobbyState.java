package com.tactics.lobby;

import java.util.ArrayList;

import com.tactics.game.logic.Team;
import com.tactics.game.unit.Unit;
import com.tactics.game.unit.UnitTypes;

public class LobbyState {
	
	ArrayList<Team> teams;
	
	public LobbyState() {
		Team newTeam = new Team("", 0);
		teams = new ArrayList<Team>();
		teams.add(newTeam);
	}
	
	public void addTeam(int id) {
		Team newTeam = new Team("", id);
		teams.add(newTeam);
	}
	
	public void addUnit(String unitType, int teamId) {
		Unit unit = UnitTypes.getUnit(unitType);
		teams.get(teamId - 1).addUnit(unit);
	}
	
	public void removeUnit(int unitId, int teamId) {
		if(teams.get(teamId - 1).getUnits().size() > 0 && unitId < teams.get(teamId-1).getUnits().size())
			teams.get(teamId-1).removeUnit(unitId);
	}
	
	public ArrayList<Team> getTeams() {
		return teams;
	}
}
