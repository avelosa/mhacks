package com.tactics.server;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.tactics.game.logic.GameLogic;
import com.tactics.game.networking.packets.GameInitializationPacket;
import com.tactics.lobby.networking.AddUnitPacket;
import com.tactics.lobby.networking.ReadyPacket;
import com.tactics.lobby.networking.RemoveUnitPacket;
import com.tactics.lobby.networking.StartPacket;

public class TacticsServerLobbyListener extends Listener {
	TacticsServer server;
	
	public TacticsServerLobbyListener(TacticsServer server) {
		this.server = server;
	}
	
	public void received (Connection c, Object object) {
		PlayerConnection connection = (PlayerConnection) c;
		server.catheckIfTeamExists(connection.getID());
		if(object instanceof ReadyPacket) {
			server.sendReadyPacket(connection);
		} else if(object instanceof StartPacket) {
			System.out.println("GOT A START PACKETSs");
			GameLogic logic = new GameLogic(server.getLobbyState().getTeams());
			server.sendInit(logic);
		} else if(object instanceof AddUnitPacket) {
			AddUnitPacket packet = (AddUnitPacket) object;
			server.getLobbyState().addUnit(packet.getUnitType(), packet.getUnitTeam());
		} else if(object instanceof RemoveUnitPacket) {
			RemoveUnitPacket packet = (RemoveUnitPacket) object;
			server.getLobbyState().removeUnit(packet.getUnitId(), packet.getUnitTeam());
		} 
	}

	public void disconnected (Connection c) {
		PlayerConnection connection = (PlayerConnection) c;
		
		//TODO End the game
	}
}
