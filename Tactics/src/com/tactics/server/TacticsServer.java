package com.tactics.server;

import java.io.IOException;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;
import com.tactics.game.logic.GameLogic;
import com.tactics.game.logic.Team;
import com.tactics.game.networking.Network;
import com.tactics.game.networking.packets.AttackRequestPacket;
import com.tactics.game.networking.packets.AttackResponsePacket;
import com.tactics.game.networking.packets.CooldownPacket;
import com.tactics.game.networking.packets.DeathPacket;
import com.tactics.game.networking.packets.GameInitializationPacket;
import com.tactics.game.networking.packets.GameLostPacket;
import com.tactics.game.networking.packets.MovementPacket;
import com.tactics.game.networking.packets.OffCooldownPacket;
import com.tactics.game.networking.packets.Packet;
import com.tactics.game.unit.Unit;
import com.tactics.lobby.LobbyState;
import com.tactics.lobby.networking.ReadyPacket;

public class TacticsServer {
	
	Server server;
	Listener listener;
	LobbyState lobbyState;
	GameLogic gameLogic;
	
	public void start() throws IOException{
		start(Network.DEFAULT_PORT);
	}

	public void start(int port) throws IOException {		
		server = new Server() {
			protected Connection newConnection () {
				Connection player = new PlayerConnection();
				//might be a bad idea
				//server.sendToAllExceptTCP(player.getID(), new PlayerJoinedPacket());
				return player;
			}
		};
		
		Network.register(server);
		
		server.start();
		server.bind(Network.DEFAULT_PORT);
		System.out.println("connected");
	}
	
	public void switchToLobby() {
		if(listener != null)
			server.removeListener(listener);
		listener = new TacticsServerLobbyListener(this);
		server.addListener(listener);
		
		lobbyState = new LobbyState();
	}
	
	public void sendInit(GameLogic logic) {
		gameLogic = logic;
			
		sendGameInitializationPacket();
	}
	
	public void switchToGame(GameLogic logic) {
		this.gameLogic = logic;
		if(listener != null)
			server.removeListener(listener);
		listener = new TacticsServerGameListener(this, gameLogic);
		server.addListener(listener);
		try {
			server.update(0);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public LobbyState getLobbyState() {
		return lobbyState;
	}
	
	public void sendMovementPacket(PlayerConnection player, Unit unit, int newX, int newY, boolean successful) {
		Packet packet = new MovementPacket(unit, newX, newY, successful);
		if(successful)
			broadcastPacket(packet);
		else
			sendPacket(packet, player);
	}
	
	public void sendDeathPacket(Unit unit) {
		Packet packet = new DeathPacket(unit);
		broadcastPacket(packet);
	}
	
	public void sendCooldownPacket(PlayerConnection player, Unit unit) {
		Packet packet = new CooldownPacket(unit);
		sendPacket(packet, player);
	}
	 
	public void sendOffCooldownPacket(Unit unit) {
		Packet packet = new OffCooldownPacket(unit);
		broadcastPacket(packet);
	}
	
	public void sendAttackResponsePacket(PlayerConnection player, AttackRequestPacket requestPacket, int attackedHealth, boolean successful){
		Packet packet = new AttackResponsePacket(requestPacket, attackedHealth, successful);
		if(successful)
			broadcastPacket(packet);
		else
			sendPacket(packet, player);
	}
	
	public void sendEndTurnPackets(PlayerConnection player, Unit unit) {
		sendOffCooldownPacket(unit);
		sendCooldownPacket(player, unit);
	}
	
	public void sendReadyPacket(PlayerConnection player) {
		System.out.println("Sending ready packet to client");
		ReadyPacket ready = new ReadyPacket(true); 
		sendPacket(ready, server.getConnections()[1]);
	}
	
	public void sendGameInitializationPacket(){
		GameInitializationPacket packet = new GameInitializationPacket(gameLogic.getTeams(), 1);
		server.sendToAllTCP(packet);
		
		packet = new GameInitializationPacket(gameLogic.getTeams(), 2);
		server.sendToAllTCP(packet);
	}
	
	public void broadcastPacket(Packet packet ) {
		server.sendToAllTCP(packet);
	}
	
	public void sendPacket(Packet packet, Connection connection) {
		server.sendToTCP(connection.getID(), packet);
	}
	
	public void catheckIfTeamExists(int id) {
		if(lobbyState.getTeams().size() <= id-1) {
			lobbyState.addTeam(id);
		}
	}
	
	public void sendGameLostPacket(Team loosers) {
		GameLostPacket packet = new GameLostPacket(loosers);
		broadcastPacket(packet);
	}
}
