package com.tactics.server;

import org.tactics.utility.MethodStatus;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.tactics.game.logic.GameLogic;
import com.tactics.game.networking.packets.AttackRequestPacket;
import com.tactics.game.networking.packets.InitializationFinishedPacket;
import com.tactics.game.networking.packets.MovementPacket;
import com.tactics.game.networking.packets.StandbyPacket;
import com.tactics.game.unit.Unit;

public class TacticsServerGameListener extends Listener {
	TacticsServer server;
	GameLogic gameLogic;
	
	public TacticsServerGameListener(TacticsServer server, GameLogic logic ) {
		this.server = server;
		gameLogic = logic;
	}
	
	public void received (Connection c, Object object) {
		PlayerConnection connection = (PlayerConnection) c;
		
		if(object instanceof AttackRequestPacket) {
			AttackRequestPacket packet = (AttackRequestPacket) object;
			Unit attacker = gameLogic.getUnitFromTeam(packet.getAttackerTeam(), packet.getAttackerId());
			Unit attacked = gameLogic.getUnitFromTeam(packet.getAttackedTeam(), packet.getAttackedId());
			
			MethodStatus status = attacker.attack(attacked, packet);
			if(status.isSuccess()) {
				int attackedHealth = attacked.getCurHealth();
				if(attackedHealth <= 0) {
					server.sendDeathPacket(attacked);
				} else{
					server.sendAttackResponsePacket(connection, packet, attacked.getCurHealth(), true);
				}
			} else {
				server.sendAttackResponsePacket(connection, packet, attacked.getCurHealth(), false);
			}
			
		} else if(object instanceof MovementPacket) {
			MovementPacket packet = (MovementPacket) object;
			Unit unit = gameLogic.getUnitFromTeam(packet.getUnitTeam(), packet.getUnitId());
			
			int newX = packet.getNewX();
			int newY = packet.getNewY();
			
			MethodStatus status = unit.move(gameLogic, newX, newY);
			if(status.isSuccess()) {
				System.out.println("SUCCESS MOVE");
				server.sendMovementPacket(connection, unit, newX, newY, true);
			} else {
				System.out.println("FAIL MOVE : " + status.getErrorMessage());
				server.sendMovementPacket(connection, unit, newX, newY, false);
			}
		} else if(object instanceof StandbyPacket) {
			StandbyPacket packet = (StandbyPacket) object;
			Unit unit = gameLogic.getUnitFromTeam(packet.getUnitTeam(), packet.getUnitId());
			unit.endTurn();
			
			server.sendEndTurnPackets(connection, unit);
		} else if(object instanceof InitializationFinishedPacket) {
			InitializationFinishedPacket packet = (InitializationFinishedPacket) object;
			
		}
	}

	public void disconnected (Connection c) {
		PlayerConnection connection = (PlayerConnection) c;
		
		//TODO End the game
	}
}
