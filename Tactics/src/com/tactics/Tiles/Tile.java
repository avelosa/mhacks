package com.tactics.Tiles;

import com.tactics.game.unit.Unit;

public class Tile {
	
	private boolean walkable;
	private Unit unit;
	private int x;
	private int y;
	
	public Tile(int x, int y, boolean walkable) {
		this.x = x;
		this.y = y;
		this.walkable = walkable;
	}
	
	public void setUnit(Unit unit) { 
		this.unit = unit;
	}
	
	public Unit getUnit() {
		return this.unit;
	}
	
	public boolean isWalkable() {
		return this.walkable;
	}
	
	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
	
	public boolean isEmpty() {
		return unit == null;
	}
	
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}
		if (o == this) {
			return true;
		}
		if (!(o instanceof Tile)) {
			return false;
		}
		Tile temp = (Tile) o;
		if (temp.getX() == this.x && temp.getY() == this.y) {
			return true;
		}
		return false;
	}
	
}
