package com.tactics.Tiles;

import java.util.HashMap;

public class TileManager {

	public static HashMap<Integer, Boolean> pathing;
	
	public TileManager() {
		pathing = new HashMap<Integer, Boolean>();
		pathing.put(14, true);
		pathing.put(41, true);
		pathing.put(42, true);
		pathing.put(44, true);
		pathing.put(45, true);
		pathing.put(46, true);
		pathing.put(47, true);
		pathing.put(53, true);
		pathing.put(54, true);
		pathing.put(55, true);
		pathing.put(56, true);
		pathing.put(57, true);
		pathing.put(58, true);
		pathing.put(59, true);
		pathing.put(60, true);
		pathing.put(67, true);
		pathing.put(68, true);
		pathing.put(69, true);
		pathing.put(79, true);
		pathing.put(80, true);
	}
	
}
