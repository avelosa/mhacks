package com.tactics.Sprites;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class SpriteBlueMask extends TacticsSprite {

	public SpriteBlueMask() {
		super(new TextureRegion(new Texture("data/bluemask.png"), 0, 0, 64, 64));
		// TODO Auto-generated constructor stub
	}

}
