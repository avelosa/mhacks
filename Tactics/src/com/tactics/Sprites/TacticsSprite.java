package com.tactics.Sprites;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public abstract class TacticsSprite extends Sprite {

	public TacticsSprite(TextureRegion region) {
		super(region);
	}
	
	public void draw(SpriteBatch spriteBatch, float x, float y) {
		this.setPosition(x, y);
		this.draw(spriteBatch);
	}
	
}
