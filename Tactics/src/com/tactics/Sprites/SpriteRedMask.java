package com.tactics.Sprites;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class SpriteRedMask extends TacticsSprite {

	public SpriteRedMask() {
		super(new TextureRegion(new Texture("data/redmask.png"), 0, 0, 64, 64));
		// TODO Auto-generated constructor stub
	}

}
