package com.tactics.Sprites;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class SpriteRedMilitia extends TacticsSprite {

	public SpriteRedMilitia() {
		super(new TextureRegion(new Texture("data/units.png"), 0, 0, 64, 64));
	}
	
}
