package com.tactics.Sprites.Icons;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.tactics.Sprites.TacticsSprite;

public class IconPurpleArcher extends TacticsSprite {

	public IconPurpleArcher() {
		super(new TextureRegion(new Texture("data/PurpleArcherIcon.png"), 0, 0, 64, 64));
		// TODO Auto-generated constructor stub
	}

}
