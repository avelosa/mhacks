package com.tactics.Sprites.Icons;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.tactics.Sprites.TacticsSprite;

public class IconPurplePaladin extends TacticsSprite{

	public IconPurplePaladin() {
		super(new TextureRegion(new Texture("data/icons.png"), 64, 64, 64, 64));
		// TODO Auto-generated constructor stub
	}

}
