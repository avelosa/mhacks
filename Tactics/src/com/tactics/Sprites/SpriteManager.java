package com.tactics.Sprites;

public class SpriteManager {

	public static TacticsSprite REDPALADIN = new SpriteRedPaladin();
	public static TacticsSprite REDMILITIA = new SpriteRedMilitia();
	public static TacticsSprite PURPLEPALADIN = new SpritePurplePaladin();
	public static TacticsSprite PURPLEMILITIA = new SpritePurpleMilitia();
	public static TacticsSprite PURPLEARCHER = new SpritePurpleArcher();
	public static TacticsSprite REDARCHER = new SpriteRedArcher();
	public static TacticsSprite OUTLINE = new SpriteOutline();
	public static TacticsSprite BLUEMASK = new SpriteBlueMask();
	public static TacticsSprite REDMASK = new SpriteRedMask();
}
