package com.tactics.Sprites;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class SpriteOutline extends TacticsSprite {

	public SpriteOutline() {
		super(new TextureRegion(new Texture("data/outline.png"), 0, 0, 64, 64));
	}

}
