package com.tactics.Sprites;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class SpriteRedPaladin extends TacticsSprite {

	public SpriteRedPaladin() {
		super(new TextureRegion(new Texture("data/units.png"), 0, 64, 64, 64));
	}
	
}
