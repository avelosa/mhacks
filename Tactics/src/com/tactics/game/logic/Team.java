package com.tactics.game.logic;

import java.util.ArrayList;

import com.tactics.game.unit.Unit;

public class Team {

	public ArrayList<Unit> units;
	public String teamName;
	public int teamNumber;
	
	public Team(String teamName, int teamNumber) {
		this.teamNumber = teamNumber;
		this.teamName = teamName;
		units = new ArrayList<Unit>();
	}
	
	public void addUnit(Unit unit) {
		unit.setId(units.size(), this);
		units.add(unit);
	}
	
	public void removeUnit(int unitId) {
		units.remove(unitId);
		for(Unit unit : units){
			if(unit.getId() > unitId)
				unit.setId(unit.getId()-1);
		}
	}
	
	public ArrayList<Unit> getUnits() {
		return this.units;
	}

	public int getTeamNumber() {
		return teamNumber;
	}

	public void setTeamNumber(int teamNumber) {
		this.teamNumber = teamNumber;
	}
	
	public String getTeamName() {
		return teamName;
	}
	
	public int getTeam() {
		return this.teamNumber;
	}
	
	public Unit getUnit(int id) {
		return units.get(id);
	}
	
	public Team copy() {
		Team newTeam = new Team(teamName, teamNumber);
		for(Unit unit : units) {
			newTeam.addUnit(unit.copy());
		}

		return newTeam;
	}
	
	public boolean isDead() {
		for(Unit unit : units) {
			if(!unit.isDead())
				return false;
		}
		
		return true;
	}
}
