package com.tactics.game.logic;

import java.util.ArrayList;

import com.tactics.Tiles.Tile;
import com.tactics.game.unit.Unit;

public class GameLogic {
	
	private ArrayList<Team> teams;
	private Tile [] [] tiles;
	
	public GameLogic(ArrayList<Team> teams) {
		this.teams = teams;
	}
	
	public void update(long interval) {
		// Check all units' cooldowns
		tickUnits(interval);
	}
	
	public void tickUnits(long interval) {

		if (teams == null) {
			return;
		}
		for (int i = 0; i < teams.size(); i++) {
			ArrayList<Unit> tempUnits = teams.get(i).getUnits();
			for (int j = 0; j < tempUnits.size(); j++) {
				tempUnits.get(j).tick(interval);
			}
		}
	}
	
	public void initialAddToMap() {
		for (int i = 0; i < teams.size(); i++) {
			Team team = teams.get(i);
			ArrayList<Unit> units = team.getUnits();
			for (int j = 0; j < units.size(); j++) {
				int x = units.get(j).getX();
				int y = units.get(j).getY();
				tiles[x][y].setUnit(units.get(j));
				units.get(j).setTile(tiles[x][y]);
			}
		}
	}
	
	public Unit getUnit(int x, int y) {
		return tiles[x][y].getUnit();
	}
	
	public ArrayList<Team> getTeams() {
		return teams;
	}
	
	public Team checkForLooser() {
		for(Team team : teams) {
			for(Unit unit : team.getUnits()) {
				if(!unit.isDead())
					break;
			}
			return team;
		}
		return null;
	}
	
	public Team getTeam(int id) {
		return teams.get(id-1);
	}
	
	public Unit getUnitFromTeam(int teamNumber, int unitId){
		return teams.get(teamNumber-1).getUnit(unitId);
	}
	
	public Tile[][] getTiles() {
		return tiles;
	}
	
	public void setTiles(Tile[][] tiles){
		this.tiles = tiles;
		initialAddToMap();
	}
	
	public ArrayList<Tile> getTilesWithinRangeIgnoreBlocks(int range, Tile start) {
		ArrayList<Tile> tiles = new ArrayList<Tile>();
		//System.out.printf("Get Tile called on %d %d with range %d\n", start.getX(), start.getY(), range);
		floodFillIgnoreBlocks(tiles, start, range);
		
		return tiles;
	}
	
	public ArrayList<Tile> getTilesWithinRange(int range, Tile start) {
		ArrayList<Tile> tiles = new ArrayList<Tile>();
		//System.out.printf("Get Tile called on %d %d with range %d\n", start.getX(), start.getY(), range);
		floodFill(tiles, start, range);
		
		return tiles;
	}
	
	private void floodFillIgnoreBlocks(ArrayList<Tile> tiles, Tile source, int range) {
		// Check the four adjacent
		if (range < 0) {
			return;
		}
		
		//System.out.printf("Flood Fill called on %d %d with range %d\n", source.getX(), source.getY(), range);
		
		int tempX = source.getX() - 1;
		int tempY = source.getY();
		Tile tempTile;
		ArrayList<Tile> tempList = new ArrayList<Tile>();
		
		if (tempX >= 0) {
			tempTile = this.tiles[tempX][tempY];
			if (!tiles.contains(tempTile)) {
				tiles.add(tempTile);
				tempList.add(tempTile);
			}
		}
		
		tempX = source.getX() + 1;
		tempY = source.getY();
		
		if (tempX < this.tiles.length) {
			tempTile = this.tiles[tempX][tempY];
			if (!tiles.contains(tempTile)) {
				tiles.add(tempTile);
				tempList.add(tempTile);
			}
		}
		
		tempX = source.getX();
		tempY = source.getY() - 1;
		
		if (tempY >= 0) {
			tempTile = this.tiles[tempX][tempY];
			if (!tiles.contains(tempTile)) {
				tiles.add(tempTile);
				tempList.add(tempTile);
			}
		}
		
		tempX = source.getX();
		tempY = source.getY() + 1;
		
		if (tempY < this.tiles[0].length) {
			tempTile = this.tiles[tempX][tempY];
			if (!tiles.contains(tempTile)) {
				tiles.add(tempTile);
				tempList.add(tempTile);
			}
		}
		
		for (int i = 0; i < tempList.size(); i++) {
			floodFillIgnoreBlocks(tiles, tempList.get(i), range - 1);
		}
		
	}

	private void floodFill(ArrayList<Tile> tiles, Tile source, int range) {
		// Check the four adjacent
		if (range < 0) {
			return;
		}
		
		//System.out.printf("Flood Fill called on %d %d with range %d\n", source.getX(), source.getY(), range);
		
		int tempX = source.getX() - 1;
		int tempY = source.getY();
		Tile tempTile;
		ArrayList<Tile> tempList = new ArrayList<Tile>();
		
		if (tempX >= 0) {
			tempTile = this.tiles[tempX][tempY];
			if (!tiles.contains(tempTile) && tempTile.isEmpty() && tempTile.isWalkable()) {
				tiles.add(tempTile);
				tempList.add(tempTile);
			}
		}
		
		tempX = source.getX() + 1;
		tempY = source.getY();
		
		if (tempX < this.tiles.length) {
			tempTile = this.tiles[tempX][tempY];
			if (!tiles.contains(tempTile) && tempTile.isEmpty() && tempTile.isWalkable()) {
				tiles.add(tempTile);
				tempList.add(tempTile);
			}
		}
		
		tempX = source.getX();
		tempY = source.getY() - 1;
		
		if (tempY >= 0) {
			tempTile = this.tiles[tempX][tempY];
			if (!tiles.contains(tempTile) && tempTile.isEmpty() && tempTile.isWalkable()) {
				tiles.add(tempTile);
				tempList.add(tempTile);
			}
		}
		
		tempX = source.getX();
		tempY = source.getY() + 1;
		
		if (tempY < this.tiles[0].length) {
			tempTile = this.tiles[tempX][tempY];
			if (!tiles.contains(tempTile) && tempTile.isEmpty() && tempTile.isWalkable()) {
				tiles.add(tempTile);
				tempList.add(tempTile);
			}
		}
		
		for (int i = 0; i < tempList.size(); i++) {
			floodFill(tiles, tempList.get(i), range - 1);
		}
		
	}
}
