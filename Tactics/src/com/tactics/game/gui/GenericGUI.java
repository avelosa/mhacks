package com.tactics.game.gui;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class GenericGUI extends Sprite{
	
	public GenericGUI(Sprite sprite) {
		super(sprite);
	}
	
	public void draw(SpriteBatch spriteBatch) {
		super.draw(spriteBatch);
	}

}
