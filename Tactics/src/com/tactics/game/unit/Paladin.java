package com.tactics.game.unit;

import com.tactics.Sprites.SpriteManager;
import com.tactics.game.logic.Team;

public class Paladin extends Unit {

	public Paladin() {
		super(100, 1, 5, 2200, 1, UnitTypes.Paladin);
	}

	public void setId(int id, Team team) {
		this.id = id;
		this.team = team;
		
		if (team.getTeamNumber() == 1) {
			setSprite(SpriteManager.REDPALADIN);
		} else {
			setSprite(SpriteManager.PURPLEPALADIN);
		}
	}
	
	@Override
	public Unit copy() {
		// TODO Auto-generated method stub
		Unit unit = new Paladin();
		unit.setX(getX());
		unit.setY(getY());
		return unit;
	}
}
