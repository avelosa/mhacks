package com.tactics.game.unit;

public enum UnitTypes {
	Archer,
	Militia,
	Paladin;
	
	public static Unit getUnit(String type) {
		if(Archer.toString().equals(type)) {
			return new Archer(0);
		} else if(Militia.toString().equals(type)) {
			return new Militia();
		} else if(Paladin.toString().equals(type)) {
			return new Paladin();
		}
		
		return null;
	}
}
