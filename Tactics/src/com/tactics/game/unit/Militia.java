package com.tactics.game.unit;

import com.tactics.Sprites.SpriteManager;
import com.tactics.game.logic.Team;

public class Militia extends Unit {

	public Militia() {
		super(60, 5, 5, 2500, 1, UnitTypes.Militia);
	}
	
	public void setId(int id, Team team) {
		this.id = id;
		this.team = team;
		
		if (team.getTeamNumber() == 1) {
			setSprite(SpriteManager.REDMILITIA);
		} else {
			setSprite(SpriteManager.PURPLEMILITIA);
		}
	}

	@Override
	public Unit copy() {
		Unit unit = new Militia();
		unit.setX(getX());
		unit.setY(getY());
		return unit;
	}

}
