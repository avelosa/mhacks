package com.tactics.game.unit;

import org.tactics.utility.MethodStatus;
import org.tactics.utility.Vector;

import com.tactics.Sprites.TacticsSprite;
import com.tactics.Tiles.Tile;
import com.tactics.game.logic.GameLogic;
import com.tactics.game.logic.Team;
import com.tactics.game.networking.packets.AttackRequestPacket;

public abstract class Unit {
	//base stats
	private int hp;
	private int attack;
	private int movespeed;
	private long cooldown;
	protected int id;
	private int attackRange;
	private UnitTypes type;

	//position
	private int x;
	private int y;

	//current status
	private int curHealth;
	private long curCooldown;
	private boolean isDead;
	private boolean canAct;
	private boolean canMove;
	private boolean canAttack;


	//TODO Set this somewhere
	protected Team team;
	private boolean selected;
	private TacticsSprite sprite;
	private Tile tile;


	public Unit(int hp, int attack, int movespeed, int cooldown, int attackRange, UnitTypes type) {
		this.hp = hp;
		this.attack = attack;
		this.movespeed = movespeed;
		this.cooldown = cooldown;
		this.attackRange = attackRange;

		curHealth = this.hp;
		curCooldown = cooldown;
		isDead = false;

		canAct = false;
		canMove = false;
		canAttack = false;
		selected = false;
		
		this.type = type;
	}
	
	public abstract Unit copy();
	
	public UnitTypes getType() {
		return type;
	}

	public void select() {
		selected = true;
		System.out.println("Hey, I got selected!");
	}

	public void deselect() {
		selected = false;
	}

	public void tick(long interval) {
		if (curCooldown <= 0){
			canAct = true;
			canMove = true;
			canAttack = true;

			curCooldown = cooldown;
		} else if(!canAct) {
			curCooldown -= interval;	
		}
	}

	public boolean canAct() {
		return this.canAct;
		//return false;
	}

	public MethodStatus endTurn() {
		canAct = false;
		canMove = false;
		canAttack = false;

		return new MethodStatus();
	}

	public MethodStatus attack(Unit attacked, AttackRequestPacket requestPacket) {
		MethodStatus status = new MethodStatus();
		Vector dist = new Vector(x, y, attacked.getX(), attacked.getY());
		if(dist.getLength() <= attackRange && canAttack) {
			attacked.takeDamage(attack);
			endTurn();
		} else {
			status = new MethodStatus("Not close enough to attack");
		}
		
		return status;
	}

	public void goToStandby() {
		endTurn();
	}

	public MethodStatus move(GameLogic logic, int newX, int newY) {
		return move(logic, new Vector(x, y, newX, newY));
	}

	public MethodStatus moveDelta(GameLogic logic, int diffX, int diffY) {
		return move(logic, new Vector(x, y, x+diffX, y+diffY));
	}

	public MethodStatus move(GameLogic gameLogic, Vector vector) {
		//just can't move
		if(!canAct || !canMove)
			return new MethodStatus("The unit cannot currently move");

		//move location out of range
		if(vector.getLength() > movespeed ) {
			return new MethodStatus("The unit cannot move that far");
		}

		//TODO check if tile is movable

		//all checks cleared
		this.x = vector.getX2();
		this.y = vector.getY2();
		this.setTile(gameLogic.getTiles()[x][y]);
		
		canMove = false;

		//TODO notify server/tiles of move
		
		return new MethodStatus();
	}
	
	public int getRange() {
		return this.attackRange;
	}
	
	public Tile getTile() {
		return this.tile;
	}

	public MethodStatus teleport(GameLogic gameLogic, int x, int y) {
		this.x = x;
		this.y = y;
		
		this.setTile(gameLogic.getTiles()[x][y]);
		
		return new MethodStatus();
	}

	public MethodStatus takeDamage(int damage) {
		curHealth = curHealth - damage;
		if(curHealth <= 0) {
			isDead = true;
		}
		
		return new MethodStatus();
	}

	public void setId(int id, Team team) {
		this.id = id;
		this.team = team;
	}
	
	public void setId(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public int getHp() {
		return hp;
	}

	public void setHp(int hp) {
		this.hp = hp;
	}

	public int getAttack() {
		return attack;
	}

	public void setAttack(int attack) {
		this.attack = attack;
	}

	public int getMovespeed() {
		return movespeed;
	}

	public void setMovespeed(int movespeed) {
		this.movespeed = movespeed;
	}

	public long getCooldown() {
		return cooldown;
	}

	public void setCooldown(int cooldown) {
		this.cooldown = cooldown;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public void setTile(Tile tile) {
		
		if (this.tile != null) {
			this.tile.setUnit(null);
		}
		this.canMove = false;
		this.tile = tile;
		tile.setUnit(this);
		//this.x = tile.getX();
		//this.y = tile.getY();
	}
	
	public int getCurHealth() {
		return curHealth;
	}

	//setter used for syncing client with server
	public void setCurHealth(int curHealth) {
		this.curHealth = curHealth;
	}

	public long getCurCooldown() {
		return curCooldown;
	}

	//setter used for syncing client w/ server
	public void setCurCooldown(long curCooldown) {
		this.curCooldown = curCooldown;
	}

	public boolean isDead() {
		return isDead;
	}
	
	public void setDead(boolean dead) {
		isDead = dead;
	}

	//setter used for syncing client w/ server
	public void setIsDead(boolean isDead) {
		this.isDead = isDead;
	}

	public Team getTeam() {
		return team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}

	public void setCanAct(boolean canAct) {
		this.canAct = canAct;
	}

	public boolean canMove() {
		return canMove;
	}

	public void setCanMove(boolean canMove) {
		this.canMove = canMove;
	}

	public boolean canAttack() {
		return canAttack;
	}

	public void setCanAttack(boolean canAttack) {
		this.canAttack = canAttack;
	}

	public void setSprite(TacticsSprite sprite) {
		this.sprite = sprite;
	}

	public TacticsSprite getSprite() {
		return this.sprite;
	}
}
