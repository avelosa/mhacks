package com.tactics.game.unit;

import com.tactics.Sprites.SpriteManager;
import com.tactics.game.logic.Team;

public class Archer extends Unit {

	public Archer(int team) {
		super(40, 10, 5, 2500, 2, UnitTypes.Archer);
	}
	
	public void setId(int id, Team team) {
		this.id = id;
		this.team = team;
		
		if (team.getTeamNumber() == 1) {
			setSprite(SpriteManager.REDARCHER);
		} else {
			setSprite(SpriteManager.PURPLEARCHER);
		}
	}

	@Override
	public Unit copy() {
		Unit unit = new Archer(getTeam().getTeamNumber());
		unit.setX(getX());
		unit.setY(getY());
		return unit;
	}

}
