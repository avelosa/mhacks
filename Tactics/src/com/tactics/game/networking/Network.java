package com.tactics.game.networking;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.EndPoint;
import com.tactics.game.networking.packets.AttackRequestPacket;
import com.tactics.game.networking.packets.AttackResponsePacket;
import com.tactics.game.networking.packets.CooldownPacket;
import com.tactics.game.networking.packets.DeathPacket;
import com.tactics.game.networking.packets.GameInitializationPacket;
import com.tactics.game.networking.packets.GameLostPacket;
import com.tactics.game.networking.packets.MovementPacket;
import com.tactics.game.networking.packets.OffCooldownPacket;
import com.tactics.game.networking.packets.StandbyPacket;
import com.tactics.lobby.networking.AddUnitPacket;
import com.tactics.lobby.networking.PlayerJoinedPacket;
import com.tactics.lobby.networking.PlayerLeftPacket;
import com.tactics.lobby.networking.ReadyPacket;
import com.tactics.lobby.networking.RemoveUnitPacket;
import com.tactics.lobby.networking.StartPacket;

public class Network {

	static public int DEFAULT_PORT = 54555;
	static public String DEFAULT_IP = "localhost";
	
	// This registers objects that are going to be sent over the network.
	static public void register (EndPoint endPoint) {
		Kryo kryo = endPoint.getKryo();
		//kryo.register(StartGamePacket.class);
		//Game packets
		kryo.register(AttackRequestPacket.class);
		kryo.register(AttackResponsePacket.class);
		kryo.register(CooldownPacket.class);
		kryo.register(OffCooldownPacket.class);
		kryo.register(MovementPacket.class);
		kryo.register(DeathPacket.class);
		kryo.register(StandbyPacket.class);
		kryo.register(GameLostPacket.class);
		
		//Lobby Packets
		kryo.register(PlayerJoinedPacket.class);
		kryo.register(PlayerLeftPacket.class);
		kryo.register(ReadyPacket.class);
		kryo.register(StartPacket.class);
		kryo.register(AddUnitPacket.class);
		kryo.register(RemoveUnitPacket.class);
		
		//Initialization Serializers
		//kryo.register(GameInitializationPacket.class, new GameInitializationSerializer());
		//kryo.register(ArrayList.class);
//		FieldSerializer<Team> team = new FieldSerializer<Team>(kryo, Team.class);
//		CollectionSerializer unitsSerializer = new CollectionSerializer();
//		unitsSerializer.setElementClass(Unit.class, new UnitSerializer());
//		
//		FieldSerializer<GameInitializationPacket> initial = new FieldSerializer<GameInitializationPacket>(kryo, GameInitializationPacket.class);
//		CollectionSerializer teamsSerializer = new CollectionSerializer();
//		teamsSerializer.setElementClass(Team.class, team);
//		teamsSerializer.setElementsCanBeNull(false);
//		
//		kryo.register(GameInitializationPacket.class, initial);
//		kryo.register(Team.class, team);
		
		kryo.register(GameInitializationPacket.class);
		kryo.register(String[].class);
//		kryo.register(Team.class);
//		kryo.register(Archer.class, new ArcherSerializer());
//		kryo.register(Militia.class, new MilitiaSerializer());
//		kryo.register(Paladin.class, new PaladinSerializer());
		
	}
}
