package com.tactics.game.networking.packets;

import com.tactics.game.unit.Unit;

public class MovementPacket implements Packet {

	public int unitId;
	//could get this from the connection class as well
	public int unitTeam;
	
	public int newX;
	public int newY;
	public boolean successful;
	
	public MovementPacket() {	
	}
	
	public MovementPacket(Unit unit, int newX, int newY) {
		this(unit, newX, newY, false);
	}
	
	public MovementPacket(Unit unit, int newX, int newY, boolean successful) {
		unitId = unit.getId();
		unitTeam = unit.getTeam().getTeamNumber();
		
		this.newX = newX;
		this.newY = newY;
		
		this.successful = successful;
		
	}
	
	public int getUnitId() {
		return unitId;
	}

	public int getUnitTeam() {
		return unitTeam;
	}

	public int getNewX() {
		return newX;
	}

	public int getNewY() {
		return newY;
	}

	public boolean isSuccessful() {
		return successful;
	}
}
