/**
 * 
 */
package com.tactics.game.networking.packets;

/**
 * @author chris
 *
 */
public final class InitializationFinishedPacket implements Packet {
	public int teamId;
	
	public InitializationFinishedPacket(){
	}
	
	public InitializationFinishedPacket(int teamId) {
		this.teamId = teamId;
	}
}
