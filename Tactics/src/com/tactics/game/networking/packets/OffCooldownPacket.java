package com.tactics.game.networking.packets;

import com.tactics.game.unit.Unit;

public class OffCooldownPacket implements Packet {
 
		public int unitId;
		public int unitTeam;
		
		boolean offCooldown;
		
		public OffCooldownPacket(){
			
		}
		
		public OffCooldownPacket(Unit unit) {
			unitId = unit.getId();
			unitTeam = unit.getTeam().getTeamNumber();
			
			offCooldown = unit.canAct();
		}

		public int getUnitId() {
			return unitId;
		}

		public int getUnitTeam() {
			return unitTeam;
		}

		public boolean isOffCooldown() {
			return offCooldown;
		}
}
