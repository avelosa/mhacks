package com.tactics.game.networking.packets;

import com.tactics.game.unit.Unit;

public class AttackRequestPacket implements Packet {
	public int attackerTeam;
	public int attackerId;
	
	public int attackedTeam;
	public int attackedId;
	
	public AttackRequestPacket() {
		
	}
	
	public AttackRequestPacket(int attackerTeam, int attackerId,
			int attackedTeam, int attackedId) {
		super();
		
		this.attackerTeam = attackerTeam;
		this.attackerId = attackerId;
		this.attackedTeam = attackedTeam;
		this.attackedId = attackedId;
	}
	
	public AttackRequestPacket(Unit attacker, Unit attacked) {
		this(attacker.getTeam().getTeamNumber(), attacker.getId(), 
				attacked.getTeam().getTeamNumber(), attacked.getId());
	}

	public int getAttackerTeam() {
		return attackerTeam;
	}

	public int getAttackerId() {
		return attackerId;
	}

	public int getAttackedTeam() {
		return attackedTeam;
	}

	public int getAttackedId() {
		return attackedId;
	}
}
