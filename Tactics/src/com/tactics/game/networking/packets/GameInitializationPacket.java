package com.tactics.game.networking.packets;

import java.util.ArrayList;

import com.tactics.game.logic.Team;
import com.tactics.game.unit.Unit;
import com.tactics.game.unit.UnitTypes;

public class GameInitializationPacket implements Packet {
	public int teamId;
	public String[] units;
	
	public GameInitializationPacket() {
	}
	
	public GameInitializationPacket(ArrayList<Team> teams, int teamId) {
		this.teamId = teamId;
		Team team = teams.get(teamId-1);
		ArrayList<String> names = new ArrayList<String>();
		for(Unit unit : team.getUnits()) {
			names.add(unit.getType().toString());
		}
		
		units = new String[names.size()];
		
		names.toArray(units);
	}
	
	public Team getTeam() {
		Team team = new Team("", teamId);
		for(String name : units) {
			team.addUnit(UnitTypes.getUnit(name));
		}
		
		return team;
	}
	
	public int getTeamId() {
		return teamId;
	}
}
