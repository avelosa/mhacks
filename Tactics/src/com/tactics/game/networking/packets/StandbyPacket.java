package com.tactics.game.networking.packets;

import com.tactics.game.unit.Unit;

public class StandbyPacket implements Packet {
	  
	public int unitId;
	public int unitTeam;

	public StandbyPacket() {
	}
	
	public StandbyPacket(Unit unit) {
		unitId = unit.getId();
		unitTeam = unit.getTeam().getTeamNumber();
	}

	public int getUnitId() {
		return unitId;
	}

	public int getUnitTeam() {
		return unitTeam;
	}
}
