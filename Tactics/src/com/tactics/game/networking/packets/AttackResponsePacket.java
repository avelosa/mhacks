package com.tactics.game.networking.packets;

public class AttackResponsePacket implements Packet {
	public int attackerTeam;
	public int attackerId;
	
	public int attackedTeam;
	public int attackedId;
	//total health left
	public int attackedHealth;
	
	public boolean successful;
	
	public AttackResponsePacket() {
		
	}
	
	//Assume attacke was failed - sent to only the attacker
	public AttackResponsePacket(AttackRequestPacket request) {
		this(request, -1, false);
	}
	
	//Assume attack was succesful - sent to everyone
	public AttackResponsePacket(AttackRequestPacket request, int attackedHealth) {
		this(request, attackedHealth, true); 
	}
	
	public AttackResponsePacket(AttackRequestPacket request, int attackedHealth, boolean successful ) {
		this.attackerTeam = request.getAttackerTeam();
		this.attackerId = request.getAttackerId();
		
		this.attackedTeam = request.getAttackedTeam();
		this.attackedId = request.getAttackedId();
		
		this.successful = successful;
		this.attackedHealth = attackedHealth;
	}

	public int getAttackerTeam() {
		return attackerTeam;
	}

	public int getAttackerId() {
		return attackerId;
	}

	public int getAttackedTeam() {
		return attackedTeam;
	}

	public int getAttackedId() {
		return attackedId;
	}

	public int getAttackedHealth() {
		return attackedHealth;
	}

	public boolean isSuccessful() {
		return successful;
	}
}
