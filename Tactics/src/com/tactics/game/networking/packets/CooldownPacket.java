package com.tactics.game.networking.packets;

import com.tactics.game.unit.Unit;

public class CooldownPacket implements Packet {
	public int unitId;
	public int unitTeam;
	
	public long curCooldown;
	public boolean canAct;
	public boolean canAttack;
	public boolean canMove;
	
	public CooldownPacket(){
		
	}
	
	public CooldownPacket(Unit unit) {
		curCooldown = unit.getCurCooldown();
		canAct = unit.canAct();
		canAttack = unit.canAttack();
		canMove = unit.canMove();
		
		unitId = unit.getId();
		unitTeam = unit.getTeam().getTeamNumber();
	}

	public int getUnitId() {
		return unitId;
	}

	public int getUnitTeam() {
		return unitTeam;
	}

	public long getCurCooldown() {
		return curCooldown;
	}

	public boolean canAct() {
		return canAct;
	}

	public boolean canAttack() {
		return canAttack;
	}

	public boolean canMove() {
		return canMove;
	}

}
