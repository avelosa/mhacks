package com.tactics.game.networking.packets;

import com.tactics.game.logic.Team;

public class GameLostPacket implements Packet {
	
	public int looserTeamId;
	
	public GameLostPacket() {
	}
	
	public GameLostPacket(Team looserTeam) {
		looserTeamId = looserTeam.getTeamNumber();
	}
	
	public int getLooserTeamId() {
		return looserTeamId;
	}
}
