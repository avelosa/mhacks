package com.tactics;

import java.io.IOException;
import java.util.ArrayList;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import com.tactics.Screens.Lobby;
import com.tactics.Screens.MainMenu;
import com.tactics.Screens.Play;
import com.tactics.Sprites.SpriteManager;
import com.tactics.Tiles.TileManager;
import com.tactics.client.TacticsClient;
import com.tactics.game.logic.GameLogic;
import com.tactics.game.logic.Team;
import com.tactics.game.networking.Network;
import com.tactics.game.unit.Archer;
import com.tactics.game.unit.Unit;
import com.tactics.server.TacticsServer;

public class Tactics extends Game {

	GameLogic gameLogic;
	GameLogic serverGameLogic;
	TacticsClient client;
	TacticsServer server;
	Screen screen;
	Lobby lobby;
	
	ArrayList<Team> teamsForGame;

	public void create() {		
		setScreen(new MainMenu(this));
	}

	public void startLobby(boolean startServer, String ip, int port) {	
		//start lobby gui
		lobby = new Lobby(client, this, startServer);
		screen = lobby;
		setScreen(screen);
		if(startServer) {
			server = new TacticsServer();
			try {
				server.start();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			server.switchToLobby();
			
			client = new TacticsClient(this);
			client.connect(Network.DEFAULT_IP, port);
			client.switchToLobby();
		} else {
			client = new TacticsClient(this);
			client.connect(ip, port);
			client.switchToLobby();
		}
		
		lobby.setClient(client);
	}

	public void swapLobbyAndGame(ArrayList<Team> teams) {
		lobby.goToGame();
		teamsForGame = teams;
		
		ArrayList<Unit> units = teams.get(0).getUnits();
		System.out.println("units 1 " + units.size()); 
		for(int i = 0; i < units.size(); i ++) {
			units.get(i).setX(3);
			units.get(i).setY(i+3);
		}
		
		units = teams.get(1).getUnits();
		System.out.println("units 2 " + units.size());
		for(int i = 0; i < units.size(); i ++) {
			units.get(i).setX(25);
			units.get(i).setY(i+3);
		}
	}

	public void startGame() {
		TileManager manager = new TileManager();
		gameLogic = new GameLogic(teamsForGame);
		if(server != null){
			ArrayList<Team> servTeamList = new ArrayList<Team>();
			for(Team team : teamsForGame) {
				Team servTeam = team.copy();
				servTeamList.add(servTeam);
			}

			serverGameLogic = new GameLogic(servTeamList);
			screen = new Play(gameLogic, client,server, serverGameLogic);
			setScreen(screen);
		} else {
			screen = new Play(gameLogic, client, null);
			setScreen(screen);
		}

		if(server != null)
			server.switchToGame(serverGameLogic);
		client.switchToGame(gameLogic);
	}

	@Override
	public void dispose() {
		super.dispose();

		SpriteManager.OUTLINE.getTexture().dispose();
		SpriteManager.REDMILITIA.getTexture().dispose();
		SpriteManager.REDPALADIN.getTexture().dispose();
	}

	@Override
	public void render() {          
		super.render();
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
	}

	@Override
	public void pause() {
		super.pause();
	}

	@Override
	public void resume() {
		super.resume();
	}
}

