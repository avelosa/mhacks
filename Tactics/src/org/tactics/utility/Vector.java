package org.tactics.utility;

public class Vector {
	//origin x,y
	int x1;
	int y1;
	
	//destination x,y
	int x2;
	int y2;
	
	public Vector(int x1, int y1, int x2, int y2) {
		this.x1 = x1;
		this.y1 = y1;
		
		this.x2 = x2;
		this.y2 = y2;
	}
	
	//diagonals = 2 length
	public int getLength() {
		int xLength = Math.abs(x2 - x1);
		int yLength = Math.abs(y2 - y1);
		
		return xLength + yLength;
	}
	
	public int getXDiff(){
		return x2 - x1;
	}
	
	public int getYDiff() {
		return y2-y1;
	}

	public int getX1() {
		return x1;
	}

	public void setX1(int x1) {
		this.x1 = x1;
	}

	public int getY1() {
		return y1;
	}

	public void setY1(int y1) {
		this.y1 = y1;
	}

	public int getX2() {
		return x2;
	}

	public void setX2(int x2) {
		this.x2 = x2;
	}

	public int getY2() {
		return y2;
	}

	public void setY2(int y2) {
		this.y2 = y2;
	}


}
