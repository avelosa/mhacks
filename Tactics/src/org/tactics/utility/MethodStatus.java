package org.tactics.utility;

public class MethodStatus {
	boolean success;
	String errorMessage;
	
	public MethodStatus(String errorMessage) {
		success = false;
		this.errorMessage = errorMessage;
	}
	
	public MethodStatus() {
		success = true;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

}
